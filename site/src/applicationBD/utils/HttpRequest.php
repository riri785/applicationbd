<?php 

namespace limagaApp\utils;

class HttpRequest {
	
	private $method; 
	private $script_name ; 
	private $path_info;  
	private $query;
	private $get;
	private $post;
	

	function __construct() {
		$this->method = $_SERVER['QUERY_STRING'] ; 
		$this->script_name = $_SERVER['SCRIPT_NAME'];
		$this->path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
		$this->query = $_SERVER['QUERY_STRING'];
		$this->get = $_GET;
		$this->post = $_POST;
		
		
	}
	
	
	public function __get( $attname ) {
		if ( property_exists ($this, $attname) ) {
			return $this->$attname ; }
		else throw new Exception ("$attname : invalid property") ;
	}

	public function __set( $attname, $attrval ) {
		if (property_exists ($this, $attname)) {
			$this->$attname = $attval ;
		return $this->$attname ; }
 
		else throw new Exception ("$attname : invalid property") ;

	}
	
	
	

}	
