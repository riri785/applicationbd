<?php 
namespace applicationBD\Model;
include_once('vendor/autoload.php') ; 

class Utilisateur extends \Illuminate\Database\Eloquent\Model{
	
	protected $table = 'utilisateurs' ; 
	protected $primaryKey = 'id' ; 
	public $timestamps = false ; 
	
	
	public function commentaires(){	
		 return $this->hasMany('\applicationBD\Model\Commentaire','email');
	}
	
}