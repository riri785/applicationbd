<?php 
namespace applicationBD\Model;
include_once('vendor/autoload.php') ; 

class Character extends \Illuminate\Database\Eloquent\Model{
	
	protected $table = 'character' ; 
	protected $primaryKey = 'id' ; 
	public $timestamps = false ; 
	
	public function games(){	
		 return $this->belongsToMany('\applicationBD\Model\Game', 'game2character', 'character_id', 'game_id');
	}
}