<?php 
namespace applicationBD\Model;
include_once('vendor/autoload.php') ; 

class platform extends \Illuminate\Database\Eloquent\Model{
	
	protected $table = 'platform' ; 
	protected $primaryKey = 'id' ; 
	public $timestamps = false ; 
	
}