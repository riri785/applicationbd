<?php 
namespace applicationBD\Model;
include_once('vendor/autoload.php') ; 

class Game extends \Illuminate\Database\Eloquent\Model{
	
	protected $table = 'game' ; 
	protected $primaryKey = 'id' ; 
	public $timestamps = false ; 
	
	public function character(){	
		 return $this->belongsToMany('\applicationBD\Model\Character', 'game2character','game_id','character_id');
	}
}