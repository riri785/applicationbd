<?php 
namespace applicationBD\Model;
include_once('vendor/autoload.php') ; 

class Commentaire extends \Illuminate\Database\Eloquent\Model{
	
	protected $table = 'commentaires' ; 
	protected $primaryKey = 'id' ; 
	public $timestamps = false ; 
	
	
	function utilisateur(){
		return $this->belongsTo('\applicationBD\Model\Utilisateur','id');
	}
	
}