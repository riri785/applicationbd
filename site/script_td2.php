<?php

/**
* SI2 
* PARIS Nicolas
* PARMENTIER Quentin
* RATH Benjamin
* WUEST Harry
*/

// TD2



include_once('vendor/autoload.php'); 
use Illuminate\Database\Capsule\Manager as DB;
use applicationBD\Model\Character ;
use applicationBD\Model\Game;
use applicationBD\Model\platform;
use applicationBD\Model\company;


$tab =  parse_ini_file('src/conf/db.etuapp.conf.ini'); 
	$username = $tab[ 'db_user' ] ; 
	$mdp = $tab[ 'db_password' ] ; 
	$dbn = $tab['dbname'] ; 
	$db = new DB();
	$db->addConnection(array (
	 'driver' => 'mysql',
	 'host' => 'localhost',
	 'database' => $dbn,
	 'username' => $username ,
	 'password' => $mdp ,
	 'charset' => 'utf8',
	 'collation' => 'utf8_unicode_ci',
	 'prefix' => ''
	));
	$db->setAsGlobal();
	$db->bootEloquent();
	
// Question 1 * Lister les jeux , afficher leur nom et deck *

	$i =0;
	$listg1 = Game::take(1000)->get();
	$nb=Game::count();
	while($i < $nb){
		
			foreach($listg1 as $u){
				echo $u->name.'<br>';	
				echo $u->deck.'<br>';
			}
		
		$i++;
		$listg1= Game::take(1000)->skip(1000*$i)->get();
	}
	
// Question 2 * Idem, mais lister 442 jeux à partir du 21173eme *

$listg2 = Game::take(442)->skip(21172)->get();
		
			foreach($listg2 as $v){
				echo $v->id.' ';	
				echo $v->name.'<br>';	
			}

			
// Question 3 * Lister les jeux dont le nom contient 'Mario' *


$listg3 = Game::where('name','LIKE','%mario%')->get();
	
		
			foreach($listg3 as $w){
				echo $w->name.'<br>';	
			}
			
			
// Question 4 * Lister les compagnies installés au Japon *

$listg4 = company::where('location_country','=','Japan')->get();
$var=company::count();

	foreach($listg4 as $x){
			echo $x->name.' ';
			echo $x->id.'<br>';
		}
		
		
// Question 5 * Lister les plateformes dont la base installées est >= 10 000 000 * 


	$listg5 = platform::where('install_base','>=','10000000')->get();

			foreach($listg5 as $y){
				echo $y->name.' ';	
				echo $y->install_base.'<br>';	
			}

