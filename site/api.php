<?php

require_once ('src/applicationBD/Fakermaster/src/autoload.php');

include_once('vendor/autoload.php'); 

require 'vendor/slim/slim/Slim/Slim.php'; 

//require('Slim/Slim.php');

\Slim\Slim::registerAutoloader();

use Illuminate\Database\Capsule\Manager as DB;


use applicationBD\Model\Game;
use applicationBD\Model\Commentaire;
use applicationBD\Model\Utilisateur;


$tab =  parse_ini_file('src/conf/db.etuapp.conf.ini'); 
	$username = $tab[ 'db_user' ] ; 
	$mdp = $tab[ 'db_password' ] ; 
	$dbn = $tab['dbname'] ; 
	$db = new DB();
	$db->addConnection(array (
	 'driver' => 'mysql',
	 'host' => 'localhost',
	 'database' => $dbn,
	 'username' => $username ,
	 'password' => $mdp ,
	 'charset' => 'utf8',
	 'collation' => 'utf8_unicode_ci',
	 'prefix' => ''
	));
	$db->setAsGlobal();
	$db->bootEloquent();
 
	$app = new \Slim\Slim();
	/** PARTIE I) **/
	
	$app->get('/games/:id', function ($id) {
		$game = Game::where('id','=',$id)->get();
		foreach($game as $g){
				echo json_encode($g);
			}
	});
	
	/** PARTIE II) et Partie III)	**/
	$app->get('/games', function ()  use ($app) {
		
		$numPage = $app->request()->params('page');
		
		$listg = Game::take(200)->skip($numPage*200)->get();
		
		$numPageNext = $numPage+1;
		$numPagePrev = $numPage-1;
		
		//pour éviter d'avoir des numéros de pages incohérents
		if($numPagePrev < 0){
			$numPagePrev = 0;
		}
		if($numPageNext> Game::count()){
			$numPageNext = Game::count();
		}
		
		
		echo "{\"games\": <br>".json_encode($listg).", <br>
		\"links\":{<br>
          \"prev\":{\"href\":\"/api/games?page=".$numPagePrev."\"},<br>
		  \"next\":{\"href\":\"/api/games?page=".$numPageNext."\"}<br>
		}";
	});
	
	/** PARTIE IV)	**/
	//j'utilise json_encode donc je ne voie pas comment ajoute un élément en plus, ou alors il ne fallait pas utiliser json_encode ??
		
	/** PARTIE V)	**/	
	//Attention, il se peux que pour certains commentaires, id de l'utilisateur correspond a un id d'une personne qui n est pas dans la base, car insérer tout le monde dans la base prend trop de temps.	
	$app->get('/games/:id/comments', function ($id) {
		$game = Game::where('id','=',$id)->get();
		foreach($game as $g){
			$game = $g;
		}
		
		$tabComment = Array();
		
		$comm = Commentaire::where('idJeu','=',$id)->get();
		foreach($comm as $c){			
			$user = Utilisateur::where('id','=',$c->idUtilisateur)->get();
			foreach($user as $u){
				$user = $u;
				var_dump($u);
			}
			
			//si il y a une erreur ici voir le commantaire juste au dessus 
			 array_push( $tabComment, $c->id);
			 array_push( $tabComment, $c->texte);
			 array_push( $tabComment, $user->pseudo);
			
		}
		
		
		
		
	});
		
	$app->run();
	


	
	
	
	
	
	
	
	
	
	

	

