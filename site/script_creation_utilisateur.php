<?php

include_once('vendor/autoload.php'); 

use Illuminate\Database\Capsule\Manager as DB;

use applicationBD\Model\Utilisateur;
use applicationBD\Model\Commentaire;

$tab =  parse_ini_file('src/conf/db.etuapp.conf.ini'); 
	$username = $tab[ 'db_user' ] ; 
	$mdp = $tab[ 'db_password' ] ; 
	$dbn = $tab['dbname'] ; 
	$db = new DB();
	$db->addConnection(array (
	 'driver' => 'mysql',
	 'host' => 'localhost',
	 'database' => $dbn,
	 'username' => $username ,
	 'password' => $mdp ,
	 'charset' => 'utf8',
	 'collation' => 'utf8_unicode_ci',
	 'prefix' => ''
	));
	$db->setAsGlobal();
	$db->bootEloquent();
	
	
	$user1 = new Utilisateur();
	$user2 = new Utilisateur();
	$user1->email = "tata.toto@laposte.net";
	$user2->email = "tutu.titi@laposte.net";
	
	$user1->mdp = password_hash("mdp1", PASSWORD_DEFAULT,array('cost'=> 12));

	$user2->mdp = password_hash("mdp2", PASSWORD_DEFAULT,array('cost'=> 12));
	
	$user1->save();
	$user2->save();
	
	$commentaire1 = new Commentaire();
	$commentaire2 = new Commentaire();
	$commentaire3 = new Commentaire();
	
	$commentaire4 = new Commentaire();
	$commentaire5 = new Commentaire();
	$commentaire6 = new Commentaire();
	
	$commentaire1->email = "tata.toto@laposte.net";
	$commentaire2->email = "tata.toto@laposte.net";
	$commentaire3->email = "tata.toto@laposte.net";
	
	$commentaire1->text = "quelle beau jeu";
	$commentaire2->text = "quelle bon jeu-video";
	$commentaire3->text = "super beau jeu";
	
	$commentaire1->idJeu = 12342;
	$commentaire2->idJeu = 12342;
	$commentaire3->idJeu = 12342;
	
	
	$commentaire4->email = "tutu.titi@laposte.net";
	$commentaire5->email = "tutu.titi@laposte.net";
	$commentaire6->email = "tutu.titi@laposte.net";
	
	$commentaire4->text = "ce jeu est nul";
	$commentaire5->text = "pire jeu du monde";
	$commentaire6->text = "autant nul que call of duty";
	
	$commentaire4->idJeu = 12342;
	$commentaire5->idJeu = 12342;
	$commentaire6->idJeu = 12342;
	
	$commentaire1->save();
	$commentaire2->save();
	$commentaire3->save();
	$commentaire4->save();
	$commentaire5->save();
	$commentaire6->save();
	
	
	
	
	
	
	
	
	
	
	
	